%define specrelease 1%{?dist}
%if 0%{?openeuler}
%define specrelease 1
%endif

%define libname libqtermwidget5

Name:           deepin-terminal
Version:        5.9.42
Release:        %{specrelease}
Summary:        Default terminal emulation application for Deepin
License:        GPLv3+
URL:            https://github.com/linuxdeepin/%{name}
Source0:        %{url}/archive/%{version}/%{name}-%{version}.tar.gz
#Patch0:         0001-fix-qtbase-QLatin1String-QLatin1Char.patch

Provides:      deepin-terminal-data
Obsoletes:     deepin-terminal-data

BuildRequires: gcc-c++
BuildRequires: cmake3
BuildRequires: qt5-linguist

BuildRequires: dtkcore-devel
BuildRequires: dtkwidget-devel
BuildRequires: dtkcommon-devel
BuildRequires: pkgconfig(dtkgui)
BuildRequires: pkgconfig(dframeworkdbus)
BuildRequires: pkgconfig(gobject-2.0)
BuildRequires: pkgconfig(atspi-2)
BuildRequires: pkgconfig(libsecret-1)

BuildRequires: pkgconfig(Qt5Widgets)
BuildRequires: pkgconfig(Qt5Core)
BuildRequires: pkgconfig(Qt5Gui)
BuildRequires: pkgconfig(Qt5DBus)
BuildRequires: pkgconfig(Qt5Network)
BuildRequires: pkgconfig(Qt5X11Extras)
BuildRequires: lxqt-build-tools >= 0.6.0~
BuildRequires: utf8proc-devel
BuildRequires: gtest-devel
BuildRequires: gmock-devel
BuildRequires: qt5-qtbase-private-devel
BuildRequires: xcb-util-wm-devel
BuildRequires: dde-qt-dbus-factory-devel
BuildRequires: libchardet-devel
BuildRequires: uchardet-devel
BuildRequires: libicu-devel

Requires: libqtermwidget5
Requires: libqtermwidget5-data
Requires: expect
Requires: zssh
Requires: qt-at-spi
Requires: at-spi2-core

%description
%{summary}.

%package -n %{libname}
Summary:        Terminal emulator widget for Qt 5
%description  -n %{libname}
Terminal emulator widget for Qt 5 (shared libraries)
QTermWidget is a Unicode-enabled, embeddable Qt widget that can be used as
built-in console or terminal emulation widget.

%package -n %{libname}-devel
Summary:        Terminal emulator widget for Qt 5
Requires:       deepin-terminal
%description -n %{libname}-devel
Terminal emulator widget for Qt 5 (shared libraries)
QTermWidget is a Unicode-enabled, embeddable Qt widget that can be used as
built-in console or terminal emulation widget.


%package -n %{libname}-data
Summary:        Terminal emulator widget for Qt 5
%description -n %{libname}-data
Terminal emulator widget for Qt 5 (shared libraries)
QTermWidget is a Unicode-enabled, embeddable Qt widget that can be used as
built-in console or terminal emulation widget.

%prep
%autosetup -p1

sed -i 's|OWNER_EXECUTE|OWNER_EXECUTE OWNER_WRITE|' CMakeLists.txt

%build
# help find (and prefer) qt5 utilities, e.g. qmake, lrelease
export PATH=%{_qt5_bindir}:$PATH
# cmake_minimum_required version is too high
sed -i "s|^cmake_minimum_required.*|cmake_minimum_required(VERSION 3.0)|" $(find . -name "CMakeLists.txt")
mkdir build && pushd build
%cmake -DCMAKE_BUILD_TYPE=Release -DAPP_VERSION=%{version} -DVERSION=%{version}  ../
%make_build
popd

%install
%make_install -C build INSTALL_ROOT="%buildroot"

%post -n %{libname}
ldconfig

%postun -n %{libname}
ldconfig

%files
%doc README.md
%license LICENSE
%{_bindir}/%{name}
%{_datadir}/%{name}/translations/*.qm
%{_datadir}/icons/hicolor/scalable/apps/%{name}.svg
%{_datadir}/applications/%{name}.desktop
%{_datadir}/deepin-manual/manual-assets/application/deepin-terminal/terminal/

%files -n %{libname}
%{_libdir}/libterminalwidget5.so.0.14.1

%files -n %{libname}-devel
%{_includedir}/terminalwidget5/*.h
%{_libdir}/cmake/terminalwidget5/*.cmake
%{_libdir}/libterminalwidget5.so
%{_libdir}/libterminalwidget5.so.0
%{_libdir}/pkgconfig/terminalwidget5.pc

%files -n %{libname}-data
%{_datadir}/terminalwidget5/color-schemes/*.schema
%{_datadir}/terminalwidget5/color-schemes/*.colorscheme
%{_datadir}/terminalwidget5/color-schemes/historic/*.schema
%{_datadir}/terminalwidget5/kb-layouts/*.keytab
%{_datadir}/terminalwidget5/kb-layouts/historic/*.keytab
%{_datadir}/terminalwidget5/translations/*.qm

%changelog
* Mon Jul 24 2023 leeffo <liweiganga@uniontech.com> - 5.9.42-1
- upgrade to version 5.9.42

* Mon Apr 10 2023 liweiganga <liwieganga@uniontech.com> - 5.4.37-1
- update: update to 5.4.37

* Fri Apr 07 2023 yangxianzhao <yangxianzhao@unionteh.com> - 5.4.8-3
- Fix abnormal display during terminal screen splitting(issue:I6QJXY)

* Fri Dec 02 2022 liweiganga <liwieganga@uniontech.com> - 5.4.8-2
- chore: fix issue:I6446Z

* Tue Jul 19 2022 konglidong <konglidong@uniontech.com> - 5.4.8-1
- update to 5.4.8

* Tue Feb 08 2022 liweigang <liweiganga@uniontech.com> - 5.2.36-2
- fix build error

* Wed Feb 10 2021 weidong <weidong@uniontech.com> - 5.2.36-1
- update to 5.2.36

* Thu Jul 30 2020 openEuler Buildteam <buildteam@openeuler.org> - 5.0.4.1-1
- Package init
